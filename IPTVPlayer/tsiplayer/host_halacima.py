# -*- coding: utf-8 -*-

import re

import requests
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import CParsingHelper
from Plugins.Extensions.IPTVPlayer.libs.tstools import (TSCBaseHostClass,
                                                        gethostname, tscolor,
                                                        tshost, unifurl)


def getinfo():
    info_ = {}
    name = 'HalaCima'
    hst = 'https://halacima.media'
    info_['old_host'] = hst
    hst_ = tshost(name)
    if hst_ != '':
        hst = hst_
    info_['host'] = hst
    info_['name'] = name
    info_['version'] = '1.0 26/11/2023'
    info_['dev'] = 'MOHAMED_OS'
    info_['cat_id'] = '21'
    info_['desc'] = 'أفلام, مسلسلات اجنبية'
    info_['icon'] = 'https://i.ibb.co/0KNmGJh/halacima.png'
    info_['recherche_all'] = '1'
    return info_


class TSIPHost(TSCBaseHostClass, CParsingHelper):
    def __init__(self):
        TSCBaseHostClass.__init__(self, {'cookie': 'halacima.cookie'})
        self.MAIN_URL = getinfo()['host']
        self.USER_AGENT = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Mobile Safari/537.36 Edg/115.0.1901.188'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl(), 'sec-fetch-dest': 'empty', 'sec-fetch-mode': 'cors', 'sec-fetch-site': 'same-origin', 'Accept': '*/*'}
        self.cacheLinks = {}

    def showmenu(self, cItem):
        TAB = [('{}الرئــيـسيــة'.format(tscolor('\c00????00')), '', '10', 'home'), ('أفـــــلام', '', '10', 'film'), ('مــسـلـسـلات', '', '10', 'serie'), ('بـرامـج تـلـفزيـونـيـة', '/category/برامج-وتلفزة/', '20', 'tv')]
        self.add_menu(cItem, '', '', '', '', '', TAB=TAB, search=False)
        self.addDir({'import': cItem['import'], 'category': 'search', 'title': _('Search'), 'search_item': True, 'page': 1, 'hst': 'tshost', 'icon': cItem['icon']})

    def showmenu1(self, cItem):
        gnr = cItem['sub_mode']
        if gnr == 'home':
            self.HalaCima_TAB = [
                {'category': 'host2', 'title': 'الـمـضـافــ حـديـثـا', 'url': self.MAIN_URL+'/ajax/getItem?item=newly&Ajax=1', 'mode': '20'},
                {'category': 'host2', 'title': ' مـقـالاتـ مـمـيـزة', 'url': self.MAIN_URL+'/ajax/getItem?item=featured&Ajax=1', 'mode': '20'},
                {'category': 'host2', 'title': ' الاكـثــر مـشــاهـدة', 'url': self.MAIN_URL+'/ajax/getItem?item=views&Ajax=1',  'mode': '20'}]
        elif gnr == 'film':
            self.HalaCima_TAB = [
                {'category': 'host2', 'title': 'أفــلام أجنـبيـة', 'url': self.MAIN_URL+'/category/أفلام-أجنبية.html/', 'mode': '20'},
                {'category': 'host2', 'title': 'أفــلام عــربـيـة', 'url': self.MAIN_URL+'/category/أفلام-عربية.html/', 'mode': '20'},
                {'category': 'host2', 'title': 'أفــلام هنــديـة', 'url': self.MAIN_URL+'/category/أفلام-هندية.html/',  'mode': '20'},
                {'category': 'host2', 'title': 'أفــلام آسـيـويـة', 'url': self.MAIN_URL+'/category/أفلام-اسيوية.html/',  'mode': '20'},
                {'category': 'host2', 'title': ' أفــلام تـركـيـة مـتـرجـمة', 'url': self.MAIN_URL+'/category/أفلام-تركي-مترجمة.html/',  'mode': '20'},
                {'category': 'host2', 'title': 'أفــلام تـركـيـة مـدبـلـجـة', 'url': self.MAIN_URL+'/category/أفلام-تركية-مدبلجة.html/',  'mode': '20'},
                {'category': 'host2', 'title': 'أفــلام أنـمـي', 'url': self.MAIN_URL+'/category/أفلام-أنمي.html/', 'mode': '20'},
                {'category': 'host2', 'title': 'ســلاسـلــ أفـلام', 'url': self.MAIN_URL+'/category/سلاسل-أفلام-كاملة.html/', 'mode': '20'}]
        elif gnr == 'serie':
            self.HalaCima_TAB = [
                {'category': 'host2', 'title': 'مسـلـسـلات أجنـبيـة', 'url': self.MAIN_URL+'/category/مسلسلات-أجنبية.html/', 'mode': '20'},
                {'category': 'host2', 'title': 'مسـلـسـلات تـركـيـة', 'url': self.MAIN_URL+'/category/مسلسلات-تركية-مترجمة.html/',  'mode': '20'},
                {'category': 'host2', 'title': 'مسـلـسـلات تـركـيـة مدبلجة', 'url': self.MAIN_URL+'/category/مسلسلات-تركية-مدبلجة.html/',  'mode': '20'},
                {'category': 'host2', 'title': 'مسـلـسـلات مـدبـلجـة', 'url': self.MAIN_URL+'/category/مسلسلات-مدبلجة.html/', 'mode': '20'},
                {'category': 'host2', 'title': 'مسـلـسـلات تـركـيـة كـامـلـة', 'url': self.MAIN_URL+'/category/مسلسلات-تركية-كاملة.html/',  'mode': '20'},
                {'category': 'host2', 'title': 'مسـلـسـلات آسـيـويـة', 'url': self.MAIN_URL+'/category/مسلسلات-أسيوية.html/',  'mode': '20'},
                {'category': 'host2', 'title': 'مسـلـسـلات كــوريـــة', 'url': self.MAIN_URL+'/category/مسلسلات-كورية-مترجمة.html/',  'mode': '20'},
                {'category': 'host2', 'title': 'مسـلـسـلات أنـمـي', 'url': self.MAIN_URL+'/category/مسلسلات-انمي.html/', 'mode': '20'}]
        self.listsTab(self.HalaCima_TAB, {'import': cItem.get('import', ''), 'icon': cItem.get('icon', '')})

    def showitms(self, cItem):
        page = cItem.get('page', 1)
        URL = '{}page{}'.format(cItem.get('url', ''), page)
        sts, data = self.getPage(URL)
        if sts:
            Liste_els = re.findall(r'class="post">.+?<a href="([^<]+)"\s*title="([^<]+)">.+?data-original="([^<]+)"\s*alt=', data, re.S)
            for (url, titre, image) in Liste_els:
                info = self.std_title(ph.clean_html(titre), with_ep=True)
                self.addDir({'import': cItem['import'], 'category': 'host2', 'title': info.get('title_display'), 'icon': image, 'desc': info.get('desc'), 'mode': '21', 'url': url, 'good_for_fav': True, 'EPG': True, 'hst': 'tshost'})
            self.addDir({'import': cItem['import'], 'category': 'host2', 'title': '{}Next Page >>{}'.format(tscolor('\c00????00'), tscolor('\c00??????')), 'icon': cItem['icon'], 'desc': '', 'mode': '20', 'url': cItem['url'], 'page': page+1})

    def showelms(self, cItem):
        URL = cItem.get('url')
        sts, data = self.getPage(URL)
        if sts:
            if '/series/' in URL:
                if 'title="شاهد الحلقات"' in data:
                    self.addMarker({'title': '{}الـحـلـقـات'.format(tscolor('\c0000??00')), 'icon': cItem['icon'], 'desc': ''})
                    tmp = self.getDataBeetwenMarkers(data, '<a title="شاهد الحلقات">', '<div class="container">', True)[1]
                    Liste_els = re.findall('href="([^"]+)".+?<span>(.+?)</li>', tmp, re.S)
                    if Liste_els:
                        for (url, titre) in Liste_els:
                            self.addVideo({'import': cItem['import'], 'hst': 'tshost', 'url': url, 'title': ph.clean_html(titre), 'desc': '', 'icon': cItem['icon'], 'good_for_fav': True, 'EPG': True, 'hst': 'tshost'})
                if 'title="سلسلة مواسم"' in data:
                    self.addMarker({'title': '{}مـــواســم'.format(tscolor('\c0000??00')), 'icon': cItem['icon'], 'desc': ''})
                    tmp = self.getDataBeetwenMarkers(data, '<a title="سلسلة مواسم">', '<div class="container">', True)[1]
                    Liste_els = re.findall('class="seasonNum">.+?href="(.+?)" title="(.+?)">', tmp, re.S)
                    if Liste_els:
                        for (url, titre) in Liste_els:
                            info = self.std_title(ph.clean_html(titre), with_ep=True)
                            self.addDir({'import': cItem['import'], 'category': 'host2', 'title': info.get('title_display'), 'icon': cItem['icon'], 'desc': '', 'mode': '21', 'url': url, 'good_for_fav': True, 'EPG': True, 'hst': 'tshost'})
            else:
                if 'id="getMoviesCollection"' in data:
                    tmp = self.getDataBeetwenMarkers(data, 'id="getMoviesCollection"', 'class="alert_message">', True)[1]
                    Liste_els = re.findall('href="([^"]+)".+?title="([^"]+)".+?src="([^"]+)"', tmp, re.S)
                    if Liste_els:
                        for (url, titre, image) in Liste_els:
                            info = self.std_title(ph.clean_html(titre), with_ep=True)
                            self.addVideo({'import': cItem['import'], 'hst': 'tshost', 'url': url, 'title': info.get('title_display'), 'desc': '', 'icon': image, 'good_for_fav': True, 'EPG': True, 'hst': 'tshost'})
                else:
                    self.addVideo({'import': cItem['import'], 'hst': 'tshost', 'url': cItem['url'], 'title': cItem['title'], 'desc': '', 'icon': cItem['icon'], 'good_for_fav': True, 'hst': 'tshost'})

    def get_links(self, cItem):
        URL = cItem.get('url').replace("movies", "watch_movies").replace("episodes", "watch_episodes")
        urlTab = []
        sts, data = self.getPage(URL)
        PostID = re.findall(r'postID.+?"([^"]+)"', data, re.S)
        NameServer = re.findall(r"getPlayer\('(.*?)'\).+?<a href", data, re.S)
        for ServersID in NameServer:
            post_data = {'server': ServersID, 'postID': PostID[0], 'Ajax': '1'}
            sts, data_ = self.getPage('{}/ajax/getPlayer'.format(self.MAIN_URL), addParams=self.HEADER, post_data=post_data)
            if sts:
                url_dat = re.findall(r"src='([^']+)'", data_, re.S | re.IGNORECASE)
                for videoUrl in url_dat:
                    if 'megamax' in videoUrl:
                        sts, data = self.getPage(videoUrl)
                        if sts:
                            Liste_els = re.findall('"version":"([^"]+)', data.replace('&quot;', '"'), re.DOTALL)
                            if Liste_els:
                                s = requests.Session()
                                headers = {'Referer': videoUrl,
                                           'Sec-Fetch-Mode': 'cors',
                                           'X-Inertia': 'true',
                                           'X-Inertia-Partial-Component': 'web/files/mirror/video',
                                           'X-Inertia-Partial-Data': 'streams',
                                           'X-Inertia-Version': Liste_els[0]}
                                data_ = s.get(videoUrl, headers=headers).json()
                                for key in data_['props']['streams']['data']:
                                    sQual = key['label']
                                    for sLink in key['mirrors']:
                                        sHosterUrl = unifurl(sLink['link'])
                                        urlTab.append({'name': '|{}| {}'.format(gethostname(sHosterUrl, True), sQual), 'url': sHosterUrl, 'need_resolve': 1, 'type': 'local'})
                    else:
                        urlTab.append({'name': '|Watch Server| {}'.format(ServersID), 'url': unifurl(videoUrl), 'need_resolve': 1, 'type': 'local'})
            self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def SearchResult(self, str_ch, page, extra):
        URL = '{}/search/{}.html/page{}'.format(self.MAIN_URL, str_ch, page)
        sts, data = self.getPage(URL)
        if sts:
            Liste_els = re.findall(r'class="post">.+?<a href="([^<]+)"\s*title="([^<]+)">.+?data-original="([^<]+)"\s*alt=', data, re.S)
            for (url, titre, image) in Liste_els:
                info = self.std_title(ph.clean_html(titre), with_ep=True)
                self.addDir({'import': extra, 'category': 'host2', 'title': info.get('title_display'), 'icon': image, 'desc': info.get('desc'), 'mode': '21', 'url': url, 'good_for_fav': True, 'EPG': True, 'hst': 'tshost'})

    def getArticle(self, cItem):
        Desc = [('Year', 'href=".+?years.+?title="(.+?)"', '\n', ''), ('Time', 'icon-alarm f12">.+?<a>(.+?)</', "\n", ""), ("Quality", 'href=".+?quality.+?title="(.+?)"', "\n", ""),
                ('Generes', 'href=".+?genre.+?title="(.+?)"', '\n', ''), ("IMDB", 'href=".+?imdb.+?icon-star-full2 f12">.+?IMDB(.+?)</', "\n", ""), ('Story', 'class="story">(.+?)</', '\n', '')]
        desc = self.add_menu(cItem, '', 'class="singleInfo">(.+?)class="the-tags">', '', 'desc', Desc=Desc)
        if desc == '':
            desc = cItem.get('desc', '')
        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem.get('icon', '')}], 'other_info': {}}]
